
const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    private now_stage;
    private controller;

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    start () {
        this.controller = cc.find("Canvas").getComponent("data_controller");
        this.now_stage = this.controller.now_stage;
    }

    onBeginContact(contact, self, other){
        if(other.node.name == "player"){
            if(this.now_stage == "stage1"){
                this.controller.change_scene("ninja_house");
            }else if (this.now_stage == "stage2"){
                this.controller.change_scene("wizard_house");
            }
        }
    }

    // update (dt) {}
}
