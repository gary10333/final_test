// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class ene_shield extends cc.Component {

    @property({type:cc.AudioClip})
    effect_charge:cc.AudioClip = null;

    @property(cc.Prefab)
    sprint:cc.Prefab = null;

    @property(cc.Prefab)
    ene_attack:cc.Prefab = null;

    private movespeed:number = 450;

    private move:boolean = false;
    private attack:boolean = false;

    private player:cc.Node;
    private sprint_node:cc.Node;
    private ene_attack_node:cc.Node;

    private startpos:cc.Vec2;
    private endpos:cc.Vec2;
    private arrow_dir:cc.Vec2;

    private dir_change;
    private charge;
    start () {
        this.player = this.node.parent.getChildByName("player");
        this.startpos = this.node.position;
        this.endpos = this.player.position;

        

        this.dir_change = ()=>{
            this.endpos = this.player.position;
            this.startpos = this.node.position;
            let dirVec = this.endpos.sub(this.startpos);
            var angle = dirVec.signAngle(cc.v2(1,0));
            let degree = Math.floor(cc.misc.radiansToDegrees(angle));
            this.arrow_dir = dirVec;
            this.arrow_dir.normalizeSelf();
            this.node.rotation = degree;
        }

        this.charge = ()=>{
            this.schedule(this.dir_change,0.1);        
            this.scheduleOnce(()=>{
                this.unschedule(this.dir_change);
                this.move = true;
                this.attack = true;
                this.ene_attack_node = cc.instantiate(this.ene_attack);
                this.ene_attack_node.setParent(this.node);
            },3)
            this.scheduleOnce(()=>{
                this.sprint_node = cc.instantiate(this.sprint);
                this.sprint_node.setParent(this.node);
            },0.01);
        }
        this.scheduleOnce(this.charge,2);
    }

    update (dt) {
        if(this.move){
            this.node.x += dt * this.movespeed * this.arrow_dir.x;
            this.node.y += dt * this.movespeed * this.arrow_dir.y;
        }
    }

    onBeginContact(contact, self, other){
        if(other.node.group == "wall" || other.node.group == "player"){
            this.move = false;
            if(this.attack){
                this.scheduleOnce(this.charge,2);
                this.attack = false;
            }
        }
    }
}
