// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class ene_zombie_2 extends cc.Component {

    @property(cc.Prefab)
    bomb:cc.Prefab = null;
    @property(cc.Prefab)
    attack:cc.Prefab = null;

    private bomb_node:cc.Node;

    private movespeed:number = 150;
    private attack_interval:number = 1;
    public dead:boolean = false;
    public dead2:boolean = true;
    private isflick:boolean = false;
    
    private dir_change;
    private arrow_dir;
    private hand_l:cc.Node;
    private hand_r:cc.Node;
    private attack_node:cc.Node = null;

    private player:cc.Node;

    private startpos:cc.Vec2;
    private endpos:cc.Vec2;

    private move:boolean = true;
    private already_attack:boolean = false;

    private right_hand_move;
    private left_hand_move;


    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    start () {
        this.player = this.node.parent.getChildByName("player");
        this.hand_l = this.node.getChildByName("zombie_left");
        this.hand_r = this.node.getChildByName("zombie_right");
        this.bomb_node = cc.instantiate(this.bomb);
        

        this.right_hand_move = cc.sequence(
            cc.moveTo(0.2, -20,-35),cc.moveTo(0.2, 100, 0),cc.moveTo(0.2, 13, -36)
            );
        this.left_hand_move = cc.sequence(
            cc.moveTo(0.2, -20,35),cc.moveTo(0.2, 100, 0),cc.moveTo(0.2, 13, 40)
            );

        this.startpos = this.node.position;
        this.endpos = this.player.position;
        let dirVec = this.endpos.sub(this.startpos);
        var angle = dirVec.signAngle(cc.v2(1,0));
        let degree = Math.floor(cc.misc.radiansToDegrees(angle));
        this.node.rotation = degree;
        this.arrow_dir = dirVec;
        this.arrow_dir.normalizeSelf();
        
        this.dir_change = ()=>{
            this.endpos = this.player.position;
            this.startpos = this.node.position;

            let dirVec = this.endpos.sub(this.startpos);
            var angle = dirVec.signAngle(cc.v2(1,0));
            let degree = Math.floor(cc.misc.radiansToDegrees(angle));
            
            this.node.rotation = degree;
            
            this.arrow_dir = dirVec;
            if(this.arrow_dir.mag() <= 100){
                this.move = false;
            }

            this.arrow_dir.normalizeSelf();
        }

        this.schedule(this.dir_change, 0.1);
        //this.scheduleOnce(()=>{this.dead = true},3);
    }

     update (dt) {
        if(this.dead && this.dead2){
            this.unscheduleAllCallbacks();
            this.dead2 = false;
            this.node.opacity = 150;
            this.isflick = true;
            this.schedule(()=>{
               if(this.isflick){
                   this.isflick = false;
                   this.node.opacity = 255;
               }else{
                   this.isflick = !this.isflick;
                   this.node.opacity = 150;
               }
            },0.2,10,0)
            this.scheduleOnce(()=>{
                this.bomb_node.setParent(this.node.parent);
                this.bomb_node.setPosition(this.node.position);
                this.node.destroy();
            },2)
        }
        else if(this.move && !this.dead){
            this.node.x += this.arrow_dir.x * dt * this.movespeed;
            this.node.y += this.arrow_dir.y * dt * this.movespeed;
            
        }
        else if(!this.already_attack && !this.dead){
            this.already_attack = true;
            this.attack_node = cc.instantiate(this.attack);
            this.unschedule(this.dir_change);
            
            this.scheduleOnce(()=>{
                this.attack_node.setParent(this.node);
                
            },0.2);
            this.hand_l.runAction(this.left_hand_move);
            this.hand_r.runAction(this.right_hand_move);
            
            this.scheduleOnce(()=>{
                this.move = true;
                this.already_attack = false;

                this.schedule(this.dir_change, 0.1);

            },this.attack_interval);
        }
     }
}
