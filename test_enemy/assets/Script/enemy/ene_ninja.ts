// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class ene_ninja extends cc.Component {

    @property(cc.Prefab)
    smoke:cc.Prefab = null;
    @property(cc.Prefab)
    slash:cc.Prefab = null;
    @property(cc.Prefab)
    shuriken:cc.Prefab = null;
    @property(cc.Prefab)
    spell:cc.Prefab = null;
    @property(cc.Prefab)
    ninja_shadow:cc.Prefab = null;

    @property({type:cc.AudioClip})
    effect_shu:cc.AudioClip = null;
    @property({type:cc.AudioClip})
    effect_smoke:cc.AudioClip = null;
    @property({type:cc.AudioClip})
    effect_slash:cc.AudioClip = null;
    @property({type:cc.AudioClip})
    effect_hurt:cc.AudioClip = null;
    @property({type:cc.AudioClip})
    effect_spell:cc.AudioClip = null;

    @property()
    shadow:boolean = false;

    private movespeed:number = 250;

    private move:boolean = false;

    private attack_interval:number = 1.2;
    private attack_count:number = 0;

    private idle_frame:cc.SpriteFrame;
    private player:cc.Node;
    private smoke_node:cc.Node;
    private slash_node:cc.Node;
    private shuriken_node:cc.Node;
    private shadow_node:cc.Node;
    private spell_node:cc.Node;
    
    private anim;

    //functions
    private dir_change;
    private smoke_show;
    private slash_show;
    private shuri;
    private flash;
    private shadow_spell;
    
    

    private startpos:cc.Vec2;
    private endpos:cc.Vec2;
    private arrow_dir:cc.Vec2;
    private smoke_vec:cc.Vec2;
    private arrow_dir2:cc.Vec2;

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    start () {
        this.idle_frame = this.getComponent(cc.Sprite).spriteFrame;
        this.player = this.node.parent.getChildByName("player");
        this.anim = this.getComponent(cc.Animation);
        
        this.dir_change = ()=>{
            this.endpos = this.player.position;
            this.startpos = this.node.position;
            let dirVec = this.endpos.sub(this.startpos);
            var angle = dirVec.signAngle(cc.v2(1,0));
            let degree = Math.floor(cc.misc.radiansToDegrees(angle));
            this.arrow_dir = dirVec;
            this.arrow_dir.normalizeSelf();
            this.node.rotation = degree;

            let a = this.random(2);
            if(a == 0){
                this.arrow_dir2 = this.arrow_dir.rotate((0.5)*Math.PI);
            }
            else if(a == 1){
                this.arrow_dir2 = this.arrow_dir.rotate((-0.5)*Math.PI);
            }
            this.move = true;
            this.scheduleOnce(()=>{this.move = false},1);
        }

        this.smoke_show = ()=>{
            this.smoke_node = cc.instantiate(this.smoke);
            this.smoke_node.setParent(this.node.parent);
            this.smoke_node.setPosition(this.smoke_vec);
            cc.audioEngine.playEffect(this.effect_smoke,false);
        }

        this.slash_show = ()=>{
            this.anim.play("ninja_slash");
            this.scheduleOnce(()=>{
                this.slash_node = cc.instantiate(this.slash);
                this.slash_node.setParent(this.node);
                cc.audioEngine.playEffect(this.effect_slash,false);
            },0.2);
        }

        this.flash = ()=>{
            this.node.opacity = 0;
            this.schedule(this.dir_change,0.1,10,0);
            this.scheduleOnce(()=>{
                this.node.x = this.player.x + this.arrow_dir.x*70;
                
                this.node.y = this.player.y + this.arrow_dir.y*70;

                this.node.rotation += 180;
                this.smoke_vec = this.node.position;
                this.smoke_show();
                this.scheduleOnce(()=>{
                    this.node.opacity = 255;
                    this.slash_show();
                    this.ModeChange();
                },0.6);
            },1.1)
        }

        this.shuri = ()=>{
            this.schedule(this.dir_change,0.1,10,0);
            this.scheduleOnce(()=>{
                this.anim.play("ninja_shu");
                cc.audioEngine.playEffect(this.effect_shu,false);
                this.shuriken_node = cc.instantiate(this.shuriken);
                this.shuriken_node.getComponent("ninja_shuriken").index = 0;
                this.shuriken_node.setParent(this.node.parent);
                this.shuriken_node.setPosition(this.node.position);

                this.shuriken_node = cc.instantiate(this.shuriken);
                this.shuriken_node.getComponent("ninja_shuriken").index = 1;
                this.shuriken_node.setParent(this.node.parent);
                this.shuriken_node.setPosition(this.node.position);

                this.shuriken_node = cc.instantiate(this.shuriken);
                this.shuriken_node.getComponent("ninja_shuriken").index = 2;
                this.shuriken_node.setParent(this.node.parent);
                this.shuriken_node.setPosition(this.node.position);
                this.ModeChange();
            },1);
        }

        this.shadow_spell = ()=>{
            cc.audioEngine.playEffect(this.effect_spell,false);
            this.spell_node = cc.instantiate(this.spell);
            this.spell_node.getComponent("TimeToDestroy").interval = 30;
            this.spell_node.setParent(this.node);
            
            

            this.smoke_vec = cc.v2(0-480/4, 0);
            this.smoke_show();
            this.scheduleOnce(()=>{
                this.shadow_node = cc.instantiate(this.ninja_shadow);
                this.shadow_node.getComponent("TimeToDestroy").interval = 30;
                this.shadow_node.setParent(this.node.parent);
                this.shadow_node.setPosition(0-480/4, 0);
            },0.6)
            this.smoke_vec = cc.v2(0+480/3,0);
            this.smoke_show();
            this.scheduleOnce(()=>{
                this.shadow_node = cc.instantiate(this.ninja_shadow);
                this.shadow_node.getComponent("TimeToDestroy").interval = 30;
                this.shadow_node.setParent(this.node.parent);
                this.shadow_node.setPosition(0+480/3, 0);
            },0.6)
            
            this.scheduleOnce(this.ModeChange,10);
        }
        if(!this.shadow){
            this.scheduleOnce(this.shadow_spell,2);
        }
        else{
            this.ModeChange();
        }

        
        
        //this.scheduleOnce(this.shadow_spell);
    }

    random(randmax:number){
        let a = Math.floor(Math.random()*randmax);
        return a;
    }

    ModeChange()
    {
        let mode = this.random(2);

        if(this.attack_count == 13 && !this.shadow){
            this.scheduleOnce(this.shadow_spell,this.attack_interval);
            this.attack_count = 0;
        }
        else if(mode == 0){
            this.scheduleOnce(this.flash,this.attack_interval);
            this.attack_count += 1;
        }
        else if(mode == 1){
            this.scheduleOnce(this.shuri,this.attack_interval);
            this.attack_count += 1;
        }
    }

     update (dt) {
        if(this.move){
            this.node.x += dt*this.arrow_dir2.x * this.movespeed;
            this.node.y += dt*this.arrow_dir2.y * this.movespeed;
        }
     }
}
