// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class ene_archer_2 extends cc.Component {

    @property(cc.Prefab)
    arrow:cc.Prefab = null;
    @property(cc.Prefab)
    soul:cc.Prefab = null;
    @property({type:cc.AudioClip})
    effect_bow:cc.AudioClip = null;
    @property(cc.SpriteFrame)
    idle_frame:cc.SpriteFrame = null;

    //private fire_interval:number = 5; //依照難度有所改變
    private aim_interval:number = 1;//
    
    public dead:boolean = false;

    private arrow_dir_change;
    private bow_aim;

    private startpos:cc.Vec2;
    private player:cc.Node;
    private endpos:cc.Vec2;
    private arrow_dir;
    public aim:boolean = true;

    private arrow_node:cc.Node = null;
    private soul_node:cc.Node = null;
    private aim_frame:cc.SpriteFrame;
    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    start () {
        this.aim_frame = this.getComponent(cc.Sprite).spriteFrame;
        this.player = this.node.parent.getChildByName("player");
        this.startpos = this.node.position;
        this.endpos = this.player.position;
        this.soul_node = cc.instantiate(this.soul);
        

        this.arrow_dir_change = ()=>{
            this.endpos = this.player.position;
            this.startpos = this.node.position;
            let dirVec = this.endpos.sub(this.startpos);
            var angle = dirVec.signAngle(cc.v2(1,0));
            let degree = Math.floor(cc.misc.radiansToDegrees(angle));
            
            this.node.rotation = degree;
            this.arrow_dir = dirVec;
        }
        this.bow_aim = ()=>{
            this.arrow_dir.normalizeSelf();
            cc.audioEngine.playEffect(this.effect_bow,false);
            this.arrow_node = cc.instantiate(this.arrow);
            this.arrow_node.parent = this.node.parent;
            this.arrow_node.setPosition(this.node.position);
            this.getComponent(cc.Sprite).spriteFrame = this.idle_frame;
            this.scheduleOnce(()=>{this.getComponent(cc.Sprite).spriteFrame = this.aim_frame},this.aim_interval/2);
        }
        this.schedule(this.arrow_dir_change,0.1);


        this.schedule(this.bow_aim,this.aim_interval)
        //this.scheduleOnce(()=>{this.dead = true},5);
    }

    update(dt){
        if(this.dead){
            this.dead = false;
            this.unscheduleAllCallbacks();
            this.soul_node.setParent(this.node.parent);
            this.soul_node.setPosition(this.node.position);
            this.node.destroy();
        }
    }
}
