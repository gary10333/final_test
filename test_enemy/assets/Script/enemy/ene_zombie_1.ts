// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class ene_zombie_1 extends cc.Component {

    @property(cc.Prefab)
    claw:cc.Prefab = null;

    private movespeed:number = 100;
    private attack_interval:number = 2;
    
    private dir_change;
    private arrow_dir;
    private hand_l:cc.Node;
    private hand_r:cc.Node;

    private player:cc.Node;
    private claw_node:cc.Node;

    private startpos:cc.Vec2;
    private endpos:cc.Vec2;

    private move:boolean = true;
    private already_attack:boolean = false;

    private right_hand_move;
    private left_hand_move;


    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    start () {
        this.player = this.node.parent.getChildByName("player");
        this.hand_l = this.node.getChildByName("hand_2_l");
        this.hand_r = this.node.getChildByName("hand_2_r");

        this.right_hand_move = cc.sequence(
            cc.moveTo(0.5, -20,-35),cc.moveTo(0.2, 50, 0),cc.moveTo(0.5, 8, -25)
            );
        this.left_hand_move = cc.sequence(
            cc.moveTo(0.5, -20,35),cc.moveTo(0.2, 50, 0),cc.moveTo(0.5, 8, 25)
            );

        this.startpos = this.node.position;
        this.endpos = this.player.position;

        this.dir_change = ()=>{
            this.endpos = this.player.position;
            this.startpos = this.node.position;

            let dirVec = this.endpos.sub(this.startpos);
            var angle = dirVec.signAngle(cc.v2(1,0));
            let degree = Math.floor(cc.misc.radiansToDegrees(angle));
            
            this.node.rotation = degree;

            this.arrow_dir = dirVec;
            if(this.arrow_dir.mag() <= 60){
                this.move = false;
            }
            this.arrow_dir.normalizeSelf();
        }

        this.schedule(this.dir_change, 0.1);
        //this.scheduleOnce(()=>{this.move = false},3);
    }

     update (dt) {
        if(this.move){
            this.node.x += this.arrow_dir.x * dt * this.movespeed;
            this.node.y += this.arrow_dir.y * dt * this.movespeed;
            //this.hand_l.getComponent(cc.RigidBody).syncPosition(true);
            //this.hand_r.getComponent(cc.RigidBody).syncPosition(true);
        }
        else if(!this.already_attack){
            this.already_attack = true;
            this.claw_node = cc.instantiate(this.claw);
            this.unschedule(this.dir_change);

            this.hand_l.runAction(this.left_hand_move);
            this.hand_r.runAction(this.right_hand_move);
            this.scheduleOnce(()=>{
                this.claw_node.setParent(this.node);
            },0.5);
            this.scheduleOnce(()=>{
                this.move = true;
                this.already_attack = false;

                this.schedule(this.dir_change, 0.1);

                //this.scheduleOnce(()=>{this.move = false},3);
            },this.attack_interval);
        }
     }
}
