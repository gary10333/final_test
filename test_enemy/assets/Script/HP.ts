import { DH_UNABLE_TO_CHECK_GENERATOR } from "constants";

const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    @property(cc.SpriteFrame)
    life0:cc.SpriteFrame=null;

    @property(cc.SpriteFrame)
    life1:cc.SpriteFrame=null;

    @property(cc.SpriteFrame)
    life2:cc.SpriteFrame=null;

    @property(cc.SpriteFrame)
    life3:cc.SpriteFrame=null;

    @property(cc.AudioClip)
    hurt:cc.AudioClip=null;

    public life:number = 3;

    public hp:number= 100;

    private mask;
    private hpUI;
    private lifeUI;
    private controller;
    // LIFE-CY:CLE CALLBACKS:

    // onLoad () {}

    start () {
        //this.controller = this.node.parent.parent.getComponent("data_controller");
        this.mask = this.node.getChildByName("safety_mask").getComponent("safety_mask");
        this.hpUI = this.node.parent.parent.getChildByName("Main Camera").getChildByName("HPUI").getChildByName("HP");
        this.lifeUI = this.node.parent.parent.getChildByName("Main Camera").getChildByName("HPUI").getChildByName("Life");
        this.setLifeUI();
    }

    heal(){
        if(this.hp + 30 >= 100){
            this.hp = 100;
        }else{
            this.hp += 30;
        }
        this.setHPUI();
    }


    setLifeUI(){
        cc.log(this.life);
        cc.log(this.lifeUI);
        if(this.life == 0){
            this.lifeUI.getComponent(cc.Sprite).spriteFrame = this.life0;
        }else if(this.life == 1){
            this.lifeUI.getComponent(cc.Sprite).spriteFrame = this.life1;
        }else if(this.life == 2){
            this.lifeUI.getComponent(cc.Sprite).spriteFrame = this.life2;
        }else if(this.life == 3){
            this.lifeUI.getComponent(cc.Sprite).spriteFrame = this.life3;
        }else{
            this.lifeUI.getComponent(cc.Sprite).spriteFrame = this.life0;
            cc.log("lifes error");
        }
    }

    setHPUI(){
        this.hpUI.getComponent(cc.Label).string = this.hp.toString() + "%";
    }

    onBeginContact(contact, self, other){
        if(other.node.group == "ene_bullet" || other.node.group == "ene_melee"){
            let a = other.node.getComponent("ene_attack").damage;
            cc.log(other.node.name);
            if(other.node.group == "ene_bullet")
                other.node.destroy();
            if(this.mask.open){
                this.mask.mask_value -= a;
            }else{
                cc.audioEngine.playEffect(this.hurt, false);
                if(this.hp - a < 0 && this.life == 0){
                    let ctrl = cc.find("Canvas").getComponent("data_controller");
                    ctrl.change_scene("init_scene");
                    //this.controller.gameOver();
                }else if(this.hp - a < 0){
                    //this.controller.playerDie();
                    this.life--;
                    this.hp = 100;
                    this.setLifeUI();
                    this.setHPUI();
                }else{
                    this.hp -= a;
                    this.setHPUI();
                }
            }
        }
    
    }
}
