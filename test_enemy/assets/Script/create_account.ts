const {ccclass, property} = cc._decorator;

@ccclass
export default class Crerateaccount extends cc.Component {

    @property(cc.AudioClip)
    private papaer_sound:cc.AudioClip = null;

    private email = null;
    
    private username = null;

    private password = null;

    onLoad(){
        this.button_create();       
        this.button_delete(); 
    }
    start(){
        cc.audioEngine.playEffect(this.papaer_sound, false);
        this.flopping();
    }

    flopping(){
        this.schedule(function(){
            if(this.node.getChildByName("email").opacity < 255){
                this.node.getChildByName("email").opacity +=1;
                this.node.getChildByName("email_box").opacity +=1;
                this.node.getChildByName("password").opacity +=1;
                this.node.getChildByName("password_box").opacity +=1;
                this.node.getChildByName("username").opacity +=1;
                this.node.getChildByName("username_box").opacity +=1;
                this.node.getChildByName("create").opacity +=1;
            }
        }, 0.01);  
    }
    
    button_create(){
        var clickEventHandler = new cc.Component.EventHandler();
        clickEventHandler.target = this.node; // 这个 node 节点是你的事件处理代码组件所属的节点
        clickEventHandler.component = "create_account";// 这个是代码文件名
        clickEventHandler.handler = "create";
        clickEventHandler.customEventData = "";
        var button = this.node.getChildByName('create').getComponent(cc.Button);
        button.clickEvents.push(clickEventHandler);
    }

    create(){
        let self = this;
        this.email = this.node.getChildByName('email_box').getComponent(cc.EditBox).string;
        this.username = this.node.getChildByName('username_box').getComponent(cc.EditBox).string;
        this.password = this.node.getChildByName('password_box').getComponent(cc.EditBox).string;

        firebase.auth().createUserWithEmailAndPassword(this.email, this.password).then(function(){
            self.build_data();
        });

    }
    build_data(){
        let self = this;
        let username = this.username;
        let em = this.email;
        let pw = this.password;
        firebase.database().ref('/username/' + username).set({
            email: em,
            password: pw,
            life: 3,
            hp: 100,
            weapon1: 'fist',
            weapon2: 'fist',
            aimo1: 0,
            aimo2: 0,
            Aimo1: 0,
            Aimo2: 0,
            stage: "init_scene",
        }).then(function(){
            firebase.auth().signOut();
            self.node.parent.getComponent('init_scene').flag = false;
            self.node.destroy();
        })

    }

    button_delete(){
        var clickEventHandler = new cc.Component.EventHandler();
         clickEventHandler.target = this.node; // 这个 node 节点是你的事件处理代码组件所属的节点
         clickEventHandler.component = "create_account";// 这个是代码文件名
         clickEventHandler.handler = "delete";
         clickEventHandler.customEventData = "";

         var button = this.node.getChildByName('delete').getComponent(cc.Button);
         button.clickEvents.push(clickEventHandler);
    }

    delete(){
        this.node.parent.getComponent('init_scene').flag = false;
        this.node.destroy();
    }
    update(){
        if(this.node.getChildByName("email").opacity >= 254){
            this.unschedule(this.flopping);
        }
    }
}
