const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    @property(cc.Node)
    text:cc.Node = null;

    @property(cc.TextAsset)
    script1:cc.TextAsset = null;

    @property(cc.TextAsset)
    script2:cc.TextAsset = null;

    scene2: cc.Node = null;

    //@property(cc.TextAsset)
    //script3:cc.TextAsset = null;
    //@property(cc.TextAsset)
    //script4:cc.TextAsset = null;

    //private duration: number = 150;

​    isOP1:boolean = true;
    isOP2:boolean = false;
    isOP3:boolean = false;
    isOP4:boolean = false;
    isOP5:boolean = false;

    counter:number = 0;
    word_counter:number = 0;
    enable:boolean = true;
	readtext:boolean = false;
	First:boolean = true;
    array1:string[] = null;
    array2:string[] = null;
    array3:string[] = null;
    array4:string[] = null;
    array5:string[] = null;
    s1:number = 0;
    duration: number = 100;
    index:number = 0;

    start () {
        this.node.getChildByName("button").getComponent(cc.Button).interactable = false;
        this.scene2 = this.node.getChildByName("scene_333");
        cc.log(this.scene2);
        this.array1 = this.script1.text.split(""); //boss
        this.array2 = this.script2.text.split(""); //character
        //this.array3 = this.script3.text.split("");
        //this.array4 = this.script1.text.split(""); //boss
        //this.array5 = this.script2.text.split(""); //character*/
        this.sc1();
      cc.director.getPhysicsManager().enabled = true;       
    }
        

    onLoad () {
        cc.game.addPersistRootNode(this.node);
        
    }

    Load_Next_Scene_op1()
    {
        if(this.index == 0){
            if(this.isOP1)
            {
                this.isOP2 = true;
                this.isOP1 = false;
                if(this.scene2.opacity < 255){
                    this.text.getComponent(cc.Label).string = "";
                    this.scene2.opacity = 255;
                    this.index++;
                }
                this.node.getChildByName("button").opacity=0;
                this.node.getChildByName("button").getComponent(cc.Button).interactable = false;
                this.sc2();
                this.word_counter =0;
                this.counter = 0;
            }
        }
    }
    /*Load_Next_Scene_op2()
    {
        this.isOP2 = true;
        this.isOP1 = false;
        if(this.scene2.opacity < 255){
            this.text.getComponent(cc.Label).string = "";
            this.scene2.opacity += 1;
        }
    }*/

    /*Load_Scene_OP2()
    {
        this.isOP2 = true;
        this.isOP1 = false;
        cc.director.loadScene("open2");
    }*/
    /*Load_Next_Scene_op2()
    {
        cc.tween(this.node)
        .to(1,{position: cc.v2(485,317) },{easing:'cubicInOut'})
        .call(() =>  {this.Load_Scene_map1(); })
        .to(1,{position:cc.v2(-600,325)},{easing:'cubicInOut'})
        .start();
    }*/

    Load_Scene_map1()
    {
        //cc.director.loadScene("map1");
    }

    sc1()
    {
        let strLen1 = this.script1.text.length;
        let strLen2 = this.script2.text.length;

        let self = this;
        if(this.isOP1)
            {
                this.text.color = cc.color(255,0,0);
                for(let i = 0; i < strLen1; i++)
                {
                    
                    setTimeout(()=>{
                        this.text.getComponent(cc.Label).string+=this.array1[i];
                        this.word_counter += 1;
                        if(this.word_counter == strLen1){
                            this.node.getChildByName("button").getComponent(cc.Button).interactable = true;
                        }
                    },self.duration*(i));
                    self.counter+=1;
                }
                
                
            }
            
    }
    sc2()
    {
        let strLen2 = this.script2.text.length;
        cc.log(this.array2);
        let self = this;
        if(this.isOP2)
        {
        this.text.color = cc.color(255,0,0);
        for(let i = 0; i < strLen2; i++)
            {
                
                    setTimeout(()=>{
                        this.text.getComponent(cc.Label).string+=this.array2[i];
                        this.word_counter += 1;
                        if(this.word_counter == strLen2){
                            this.node.getChildByName("button").getComponent(cc.Button).interactable = true;
                        }
                        },self.duration*(i));
                        self.counter+=1;
                        if(self.counter==20)
                        {
                            this.text.getComponent(cc.Label).string+="\n";
                        }
            }

        }
    }

}
