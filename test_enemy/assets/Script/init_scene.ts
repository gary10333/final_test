const {ccclass, property} = cc._decorator;

@ccclass
export default class Menu extends cc.Component {

    @property(cc.Prefab)
    private envelope_prefab:cc.Prefab = null;

    @property(cc.Prefab)
    private login_prefab: cc.Prefab = null;

    @property(cc.Prefab)
    private new_game_prefab: cc.Prefab = null;

    @property(cc.Prefab)
    private load_game_prefab: cc.Prefab = null;

    @property(cc.AudioClip)
    private init_sound: cc.AudioClip = null;

    private signup = null;

    private login = null;

    private username;

    public flag = false;

    private next_stage;

    onLoad () {
        cc.log('a');
        cc.audioEngine.playMusic(this.init_sound, true);
        this.signupbutton();
        this.start_button();
    }

    start_button(){
        var clickEventHandler = new cc.Component.EventHandler();
        clickEventHandler.target = this.node; // 这个 node 节点是你的事件处理代码组件所属的节点
        clickEventHandler.component = "init_scene";// 这个是代码文件名
        clickEventHandler.handler = "log_in";
        clickEventHandler.customEventData = "";

        var button = this.node.getChildByName('signin_button').getComponent(cc.Button);
        button.clickEvents.push(clickEventHandler);
    }

    signupbutton(){
        var clickEventHandler = new cc.Component.EventHandler();
        clickEventHandler.target = this.node; // 这个 node 节点是你的事件处理代码组件所属的节点
        clickEventHandler.component = "init_scene";// 这个是代码文件名
        clickEventHandler.handler = "envelope";
        clickEventHandler.customEventData = "";
        var button = this.node.getChildByName('signup_button').getComponent(cc.Button);
        button.clickEvents.push(clickEventHandler);
    }

    private envelope(){
        if(this.flag == false){
            this.signup = cc.instantiate(this.envelope_prefab);
            this.signup.parent = this.node;
            this.flag = true;
        }
    }

    private log_in(){
        if(this.flag == false){
            this.login = cc.instantiate(this.login_prefab);
            this.login.parent = this.node;
            this.flag = true;
        }
    }

    newandload(){
        this.node.getChildByName("signin_button").destroy();
        this.node.getChildByName("signup_button").destroy();
        
        this.new_button();
        this.load_button();
    }
    new_button(){
        var clickEventHandler = new cc.Component.EventHandler();
        clickEventHandler.target = this.node; // 这个 node 节点是你的事件处理代码组件所属的节点
        clickEventHandler.component = "init_scene";// 这个是代码文件名
        clickEventHandler.handler = "new_game";
        clickEventHandler.customEventData = "";

        let new_but = cc.instantiate(this.new_game_prefab);
        new_but.parent = this.node;

        var button = this.node.getChildByName('new_game').getComponent(cc.Button);
        button.clickEvents.push(clickEventHandler);
    }

    new_game(){
        let self = this;
        let ctrl = self.node.parent.getComponent("data_controller");
        this.username = cc.find("Canvas").getComponent("data_controller").username;
        console.log(this.username);
        firebase.database().ref('/username/' + self.username).update({
            life: 3,
            hp: 100,
            weapon1: 'fist',
            weapon2: 'fist',
            aimo1: 0,
            aimo2: 0,
            Aimo1: 0,
            Aimo2: 0,
            stage: "stage1",
        }).then(function(){
            ctrl.change_scene("op");
        })

    }
    load_button(){
        var clickEventHandler = new cc.Component.EventHandler();
        clickEventHandler.target = this.node; // 这个 node 节点是你的事件处理代码组件所属的节点
        clickEventHandler.component = "init_scene";// 这个是代码文件名
        clickEventHandler.handler = "load_game";
        clickEventHandler.customEventData = "";

        let load_but = cc.instantiate(this.load_game_prefab);
        load_but.parent = this.node;

        var button = this.node.getChildByName('load_game').getComponent(cc.Button);
        button.clickEvents.push(clickEventHandler);
    }
    load_game(){
        let ctrl = this.node.parent.getComponent("data_controller");
        this.username = cc.find("Canvas").getComponent("data_controller").username;
        firebase.database().ref('/username').once('value',(snapshot) =>{
            var data = snapshot.val();
            snapshot.forEach((data) =>{
                console.log("data is" + data.key);
                console.log("username is " + this.username);
                if(data.key == this.username){
                    this.next_stage = data.val().stage;  
                }
                console.log("data . stage is " + this.next_stage);
            })
        }).then(() =>{
            {
                ctrl.change_scene(this.next_stage);
            }
        })
    }
}
