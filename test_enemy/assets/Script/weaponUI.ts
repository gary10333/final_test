// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class weaponUI extends cc.Component {

    @property(cc.SpriteFrame)
    fist: cc.SpriteFrame = null;

    @property(cc.SpriteFrame)
    ar: cc.SpriteFrame = null;
    
    @property(cc.SpriteFrame)
    axe: cc.SpriteFrame = null;
    
    @property(cc.SpriteFrame)
    lasergun: cc.SpriteFrame = null;
    
    @property(cc.SpriteFrame)
    gun: cc.SpriteFrame = null;
    
    @property(cc.SpriteFrame)
    knife: cc.SpriteFrame = null;
    
    @property(cc.SpriteFrame)
    samura: cc.SpriteFrame = null;
    
    @property(cc.SpriteFrame)
    shield: cc.SpriteFrame = null;
    
    @property(cc.SpriteFrame)
    shotgum: cc.SpriteFrame = null;
    
    @property(cc.SpriteFrame)
    sniper: cc.SpriteFrame = null;

    private weaponInfo;
    private weapon1:cc.Node = null;
    private weapon2:cc.Node = null;
    private amo:cc.Label = null;
    private maxAmo:number;
    private hasAmo:boolean = false;
    private stage = null;


    start () {
        
        
        //cc.log(this.weaponInfo.weapon1);
    }

    setUI(){
        //console.log("in weapon UI");
        this.stage = cc.find("Canvas").getComponent("data_controller").now_stage;
        this.weaponInfo = this.node.parent.parent.getChildByName(this.stage).getChildByName("player").getComponent("move");
        this.weapon1 = this.node.getChildByName("weapon1").getChildByName("weapon1");
        this.weapon2 = this.node.getChildByName("weapon2");
        this.amo = this.node.getChildByName("weapon1").getChildByName("amo").getComponent(cc.Label);
        this.amo.string ="";
        //cc.log("看這一行:",this.weaponInfo.weapon1, this.weaponInfo.aimol1, this.weaponInfo.Aimol1, this.weaponInfo.weapon2);
        this.maxAmo = 0;
        this.hasAmo = false
        if(this.weaponInfo.weapon1=="fist"){
            this.weapon1.getComponent(cc.Sprite).spriteFrame = this.fist;
            this.hasAmo = false;
        }else if(this.weaponInfo.weapon1 == "axe"){
            this.weapon1.getComponent(cc.Sprite).spriteFrame = this.axe;
            this.hasAmo = false;
        }else if(this.weaponInfo.weapon1 == "knife"){
            this.weapon1.getComponent(cc.Sprite).spriteFrame = this.knife;
            this.hasAmo = false;
        }else if(this.weaponInfo.weapon1 == "samura"){
            this.weapon1.getComponent(cc.Sprite).spriteFrame = this.samura;
            this.hasAmo = false;
        }else if(this.weaponInfo.weapon1 == "shield"){
            this.weapon1.getComponent(cc.Sprite).spriteFrame = this.shield;
            this.hasAmo = false;
        }else if(this.weaponInfo.weapon1 == "gun"){
            this.weapon1.getComponent(cc.Sprite).spriteFrame = this.gun;
            this.maxAmo = 6;
            this.hasAmo = true;
        }else if(this.weaponInfo.weapon1 == "shotgum"){
            this.weapon1.getComponent(cc.Sprite).spriteFrame = this.shotgum;
            this.maxAmo = 5;
            this.hasAmo = true;
        }else if(this.weaponInfo.weapon1 == "sniper"){
            this.weapon1.getComponent(cc.Sprite).spriteFrame = this.sniper;
            this.maxAmo = 1;
            this.hasAmo = true;
        }else if(this.weaponInfo.weapon1 == "ar"){
            cc.log("success");
            this.weapon1.getComponent(cc.Sprite).spriteFrame = this.ar;
            this.maxAmo = 15;
            this.hasAmo = true;
        }else if(this.weaponInfo.weapon1 == "lasergun"){
            this.weapon1.getComponent(cc.Sprite).spriteFrame = this.lasergun;
            this.maxAmo = 30;
            this.hasAmo = true;
        }
        if(this.weaponInfo.weapon2 =="fist"){
            this.weapon2.getComponent(cc.Sprite).spriteFrame = this.fist;
        }else if(this.weaponInfo.weapon2 == "axe"){
            this.weapon2.getComponent(cc.Sprite).spriteFrame = this.axe;
        }else if(this.weaponInfo.weapon2 == "knife"){
            this.weapon2.getComponent(cc.Sprite).spriteFrame = this.knife;
        }else if(this.weaponInfo.weapon2 == "samura"){
            this.weapon2.getComponent(cc.Sprite).spriteFrame = this.samura;
        }else if(this.weaponInfo.weapon2 == "shield"){
            this.weapon2.getComponent(cc.Sprite).spriteFrame = this.shield;
        }else if(this.weaponInfo.weapon2 == "gun"){
            this.weapon2.getComponent(cc.Sprite).spriteFrame = this.gun;
        }else if(this.weaponInfo.weapon2 == "shotgum"){
            this.weapon2.getComponent(cc.Sprite).spriteFrame = this.shotgum;
        }else if(this.weaponInfo.weapon2 == "sniper"){
            this.weapon2.getComponent(cc.Sprite).spriteFrame = this.sniper;
        }else if(this.weaponInfo.weapon2 == "ar"){
            this.weapon2.getComponent(cc.Sprite).spriteFrame = this.ar;
        }else if(this.weaponInfo.weapon2 == "lasergun"){
            this.weapon2.getComponent(cc.Sprite).spriteFrame = this.lasergun;
        }
        if(this.hasAmo){
            this.amo.string = this.weaponInfo.aimol1.toString() + "/" + this.maxAmo.toString() + " " + this.weaponInfo.Aimol1.toString();
        }else{
            this.amo.string = "";
        }
        //cc.log("那一行:",this.weaponInfo.weapon1, this.weaponInfo.aimol1, this.weaponInfo.Aimol1, this.weaponInfo.weapon2);
    }
    

    // update (dt) {}
}
