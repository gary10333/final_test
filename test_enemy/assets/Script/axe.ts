// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class axe extends cc.Component {


    // LIFE-CYCLE CALLBACKS:

    //todo axe on endpos
    @property(cc.Prefab)
    axe: cc.Prefab = null;

    @property(cc.Prefab)
    blood: cc.Prefab = null;

    // onLoad () {}
    private movespeed:number = 800;

    private startpos:cc.Vec2;
    private player:cc.Node;
    private endpos:cc.Vec2;

    private arrow_dir_change;
    private arrow_dir;
    private aim;
    public damage:number = 25;
    start(){
        this.aim = this.node.parent.getChildByName("aim");//todo:var for farm
        this.endpos = this.aim.position;
        this.startpos = this.node.position;
        this.arrow_dir = this.endpos.sub(this.startpos);
        this.arrow_dir.normalizeSelf();
        this.node.getComponent(cc.RigidBody).linearVelocity = this.arrow_dir.mul(this.movespeed);
        //cc.log("start pos: "+this.startpos);
        //cc.log("end pos: "+this.endpos);
    }

     update (dt) {
        this.node.rotation += 30;
        //cc.log(this.node.rotation);
        if(this.arrow_dir.x<=0&&this.arrow_dir.y<=0){
            if(this.node.x<=this.endpos.x&&this.node.y<=this.endpos.y)
            {
                var newNode = cc.instantiate(this.axe);
                newNode.setParent(this.aim.parent);
                newNode.setPosition(this.endpos);
                //cc.log("done");
                this.node.destroy();
            }
        }else if(this.arrow_dir.x<=0&&this.arrow_dir.y>=0){
            if(this.node.x<=this.endpos.x&&this.node.y>=this.endpos.y)
            {
                var newNode = cc.instantiate(this.axe);
                newNode.setParent(this.aim.parent);
                newNode.setPosition(this.endpos);
                //cc.log("done");
                this.node.destroy();
            }
        }else  if(this.arrow_dir.x>=0&&this.arrow_dir.y<=0){
            if(this.node.x>=this.endpos.x&&this.node.y<=this.endpos.y)
            {
                var newNode = cc.instantiate(this.axe);
                newNode.setParent(this.aim.parent);
                newNode.setPosition(this.endpos);
                //cc.log("done");
                this.node.destroy();
            }
        }else if(this.arrow_dir.x>=0&&this.arrow_dir.y>=0){
            if(this.node.x>=this.endpos.x&&this.node.y>=this.endpos.y)
            {
                var newNode = cc.instantiate(this.axe);
                newNode.setParent(this.aim.parent);
                newNode.setPosition(this.endpos);
                //cc.log("done");
                this.node.destroy();
            }
        }else{}
     }

     onBeginContact(contact, self, other){
        if(other.node.group=="enemy"){
            var newNode = cc.instantiate(this.blood);
            let dirx = Math.floor(Math.random()*2);
            dirx = (dirx == 1)? 1: -1;
            let diry = Math.floor(Math.random()*2);
            diry = (diry == 1)? 1: -1;
            let posx = Math.floor(Math.random()*25);
            let posy = Math.floor(Math.random()*25);
            let rotation = Math.floor(Math.random()*360);
            let scalar = 0.5 + Math.random()*0.5;
            newNode.setParent(other.node);
            newNode.setPosition(dirx*posx, diry*posy);
            newNode.setRotation(rotation);
            newNode.setScale(scalar);
            contact.disabled = true;
            setTimeout(( () => {
                newNode.destroy();
                contact.disabled = false;
            }), 200);
        }else if(other.node.group == "wall"){
            var newNode = cc.instantiate(this.axe);
            newNode.setParent(this.aim.parent);
            newNode.setPosition(self.node.position);
            //cc.log("done");
            this.node.destroy();
            
        }
     }
}
