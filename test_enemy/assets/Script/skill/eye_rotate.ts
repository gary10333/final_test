

const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    @property(cc.AudioClip)
    private eye_sound: cc.AudioClip = null;

    private degree = 0;

    private biggest:boolean = false;

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    start () {
        cc.audioEngine.playEffect(this.eye_sound,false);
    }

    update (dt) {
        if(this.node.width < 640 && this.biggest == false){
            this.node.width += 16;
            this.node.height += 16;
        }else if (this.node.width == 640 && this.biggest == false){
            this.scheduleOnce(function(){
                this.biggest = true;
             }, 5);
        }else if(this.node.width >0 && this.biggest == true){
            this.node.width -= 16;
            this.node.height -= 16;
        }else if (this.node.width == 0 && this.biggest == true){
            this.node.destroy();
        }
        this.node.x = 0;
        this.node.y = 0;
        this.degree = (this.degree+1)%360;
        this.node.rotation = this.degree;
        this.getComponent(cc.RigidBody).syncRotation(true);
    }
}
