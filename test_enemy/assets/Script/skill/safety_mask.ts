
const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    @property(cc.AudioClip)
    private mask_sound: cc.AudioClip = null;

    private player;

    public mask_value:number = 50;

    public open:boolean = false;

    public break: boolean = false;

    // onLoad () {}

    start () {
    }

    mask_open(){
        cc.audioEngine.playEffect(this.mask_sound,false);
        this.schedule(this.bigger, 0.01);
    }
    mask_close(){
        this.schedule(this.smaller, 0.01);
    }

    bigger(){
        if(this.node.width < 150 && this.open == false && this.break == false){
            this.node.width += 6;
            this.node.height += 6;
        }
        else{
            this.open = true;
            this.unschedule(this.bigger);
        }
    }
    smaller(){
        if(this.node.width > 0){
            this.node.width -= 6;
            this.node.height -= 6;
        }
        else{
            this.open = false;
            this.unschedule(this.smaller);
        }
    }

    update (dt) {
        if(this.mask_value <= 0){
            this.break = true;
            this.mask_value = 0;
            this.schedule(this.smaller, 0.01);
            this.open = false;
        }
        if(this.mask_value < 50 && this.open == false){
            this.mask_value += 0.01;
        }else if(this.mask_value >= 50){
            this.break == false;
        }
        this.node.x = 0;
        this.node.y = 0;
    }
}
