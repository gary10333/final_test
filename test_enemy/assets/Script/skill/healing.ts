const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    @property(cc.AudioClip)
    private healsound: cc.AudioClip = null;

    private anim = null; //this will use to get animation component

    private animateState = null; //this will use to record animationState

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this.anim = this.getComponent(cc.Animation);
        this.anim.play("healing");
        cc.audioEngine.playEffect(this.healsound,false);
    }

    start () {
        this.anim.on('finished',function(){
            this.node.destroy();
        }, this);
    }

    // update (dt) {}
}
