const {ccclass, property} = cc._decorator;

@ccclass
export default class Crerateaccount extends cc.Component {

    @property(cc.AudioClip)
    private papaer_sound:cc.AudioClip = null;

    private controller = null;

    private email:string = null;
    
    private password = null;

    onLoad(){
        this.button_login(); 
        this.button_delete();       
    }

    start(){
        this.controller = cc.find("Canvas").getComponent("data_controller");
        cc.audioEngine.playEffect(this.papaer_sound, false);
        this.flopping();
    }
    
    flopping(){
        this.schedule(function(){
            if(this.node.getChildByName("email").opacity < 255){
                this.node.getChildByName("email").opacity +=1;
                this.node.getChildByName("email_box").opacity +=1;
                this.node.getChildByName("password").opacity +=1;
                this.node.getChildByName("password_box").opacity +=1;
                this.node.getChildByName("login").opacity +=1;
            }
        }, 0.01);  
    }

    button_login(){
        var clickEventHandler = new cc.Component.EventHandler();
        clickEventHandler.target = this.node; // 这个 node 节点是你的事件处理代码组件所属的节点
        clickEventHandler.component = "signin";// 这个是代码文件名
        clickEventHandler.handler = "login";
        clickEventHandler.customEventData = "";
        var button = this.node.getChildByName('login').getComponent(cc.Button);
        button.clickEvents.push(clickEventHandler);


    }

    button_delete(){
        var clickEventHandler = new cc.Component.EventHandler();
         clickEventHandler.target = this.node; // 这个 node 节点是你的事件处理代码组件所属的节点
         clickEventHandler.component = "signin";// 这个是代码文件名
         clickEventHandler.handler = "delete";
         clickEventHandler.customEventData = "";

         var button = this.node.getChildByName('delete').getComponent(cc.Button);
         button.clickEvents.push(clickEventHandler);
    }

    delete(){
        this.node.parent.getComponent('init_scene').flag = false;
        this.node.destroy();
    }

    login(){
        console.log(this.node.getChildByName('email_box'));
        this.email = this.node.getChildByName('email_box').getComponent(cc.EditBox).string;
        console.log("the input email is" + this.email);
        this.password = this.node.getChildByName('password_box').getComponent(cc.EditBox).string;

        firebase.auth().signInWithEmailAndPassword(this.email, this.password).then(()=>{
            this.get_data();
        });

    }
    get_data(){

        let username = null;
        //let cocos = cc;
        firebase.database().ref('/username').once('value', (snapshot)=>{
            var data = snapshot.val();
            snapshot.forEach((data)=>{
                //console.log("data email" + data.val().email);
                //console.log("this email" + this.email);

                if(data.val().email == this.email){
                    //console.log("this email" + this.email);
                    username = data.key;
                    this.controller.username = username;
                    //console.log(this.controller.username);
                }
            })
        }).then(()=>{
            this.node.parent.getComponent("init_scene").newandload();
            this.node.destroy();
        })
    }
    update(){
        if(this.node.getChildByName("email").opacity >= 254){
            this.unschedule(this.flopping);
        }
    }
}
